﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PeBank.DataTransferObjects.Services;
using PeBank.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PeBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IMapper _mapper;
        public RoleController(RoleManager<Role> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(_roleManager.Roles.ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RoleDto model)
        {
            if (ModelState.IsValid)
            {
                Role role = new Role();
                role.Name = model.Name;
                var result = await _roleManager.CreateAsync(role);
                return Ok(result);
            }
            return BadRequest(ModelState.ValidationState);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] RoleDto model)
        {

            if (ModelState.IsValid)
            {
                Role role = new Role();
                role.Id = model.Id;
                role.Name = model.Name;
                var result = await _roleManager.UpdateAsync(role);
                if (result.Succeeded)
                {
                    return Ok(new { Status = true });
                }
                else
                {
                    return BadRequest(new { Status = false });
                }
            }
            return BadRequest(ModelState.ValidationState);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            var status = await _roleManager.DeleteAsync(role);
            if (!status.Succeeded)
            {
                return BadRequest(new { Status = false });
            }
            return StatusCode(200, new { Status = true });
        }
    }
}
