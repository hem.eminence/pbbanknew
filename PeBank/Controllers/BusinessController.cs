﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Pebank.Services.Interfaces;
using PeBank.DataTransferObjects.Services;
using PeBank.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PeBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessController : ControllerBase
    {

        private readonly IBusinessService _businessService;
        private readonly IConfiguration _config;
        string UploadPath = "";
        public BusinessController(IBusinessService businessService, IConfiguration _config)
        {
            _businessService = businessService;
            UploadPath = _config.GetSection("UploadData").GetSection("UploadPath").Value;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int PageNo, int PageSize)
        {
            try
            {
                return Ok(await _businessService.ListAsync(PageNo, PageSize));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Post( [FromForm] BusinessRequestDto model)
        {
            try
            {
                var files = Request.Form.Files;
                var folderName = UploadPath;

                bool folderExists = Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), folderName));
                if (!folderExists)
                {
                    Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), folderName));
                }
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (files.Any(f => f.Length == 0))
                {
                    return BadRequest();
                }
                foreach (var file in files)
                {
                    string GuidFile = Guid.NewGuid().ToString();
                    var extension = Path.GetExtension(file.FileName);
                    string FileName = GuidFile + extension;
                    var FileSytemName = FileName;
                    var Size = (Math.Round(file.Length / 1024f)).ToString();
                    int lastIndexFilename = file.FileName.LastIndexOf(".");
                    var fullPath = Path.Combine(pathToSave, FileName);
                    model.Logo = Path.Combine(folderName, FileName); //you can add this path to a list and then return all dbPaths to the client if require
                    string Extensionsubtring = extension.Substring(0 + 1);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }

                var isExist = await _businessService.GetDetailByRIN(model.RIN);
                if (isExist == 0)
                {
                    return Ok(await _businessService.InsertAsync(model));
                }
                else
                {
                    return BadRequest("This Rin Value Already exist.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(int id, [FromBody] BusinessRequestDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                var businessFromRepo = await _businessService.GetDetailById(id);

                if (businessFromRepo == null)
                {
                    return StatusCode(404, "Record does not exist.");
                };
                var isExist = await _businessService.GetDetailByRIN(model.RIN);
                if (isExist == 0 || isExist == model.Id)
                {
                    var status = await _businessService.UpdateAsync(model);
                    if (!status)
                    {
                        return BadRequest(new { Status = false });
                    }
                }
                else
                {
                    return BadRequest("This Rin Value Already exist.");
                }

                return Ok(new { Status = true });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Status = false, Category = ex.Message.ToString() });
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var status = await _businessService.DeleteAsync(id);
            if (!status)
            {
                return BadRequest(new { Status = false });
            }
            return StatusCode(200, new { Status = true });
        }
    }
}
