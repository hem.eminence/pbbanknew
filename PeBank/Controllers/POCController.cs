﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Pebank.Services.Interfaces;
using PeBank.DataTransferObjects.Services;
using System;
using System.Threading.Tasks;

namespace PeBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class POCController : ControllerBase
    {
        private readonly IPOCService _POCService;
        public POCController(IPOCService POCService)
        {
            _POCService = POCService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int PageNo, int PageSize)
        {
            try
            {
                return Ok(await _POCService.ListAsync(PageNo, PageSize));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] POCDto model)
        {
            try
            {
                return Ok(await _POCService.InsertAsync(model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(int id, [FromBody] POCDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                var POCFromRepo = await _POCService.GetDetailById(id);
                if (POCFromRepo == null)
                {
                    return StatusCode(404, "Record does not exist.");
                };
                var status = await _POCService.UpdateAsync(model);

                if (!status)
                {
                    return BadRequest(new { Status = false });
                }
                return Ok(new { Status = true });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Status = false, POC = ex.Message.ToString() });
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var status = await _POCService.DeleteAsync(id);
            if (!status)
            {
                return BadRequest(new { Status = false });
            }
            return StatusCode(200, new { Status = true });
        }
    }
}
