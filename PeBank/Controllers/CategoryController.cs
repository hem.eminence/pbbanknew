﻿using Microsoft.AspNetCore.Mvc;
using Pebank.Services.Interfaces;
using PeBank.DataTransferObjects.Services;
using System;
using System.Threading.Tasks;

namespace PeBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(await _categoryService.ListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryDto model)
        {
            try
            {
                return Ok(await _categoryService.InsertAsync(model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(int id, [FromBody] CategoryDto model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                var categoryFromRepo = await _categoryService.GetDetailById(id);
                if (categoryFromRepo == null)
                {
                    return StatusCode(404, "Record does not exist.");
                };
                var status = await _categoryService.UpdateAsync(model);

                if (!status)
                {
                    return BadRequest(new { Status = false });
                }
                return Ok(new { Status = true });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Status = false, Category = ex.Message.ToString() });
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var status = await _categoryService.DeleteAsync(id);
            if (!status)
            {
                return BadRequest(new { Status = false });
            }
            return StatusCode(200, new { Status = true });
        }
    }
}
