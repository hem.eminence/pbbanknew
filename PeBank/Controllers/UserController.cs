﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Pebank.Services.Interfaces;
using PeBank.DataTransferObjects.Services;
using PeBank.Entities;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PeBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ICityService _cityService;
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _config;
        private readonly IPOCService _pocService;
        public UserController(IUserService userService, ICityService cityService, ICategoryService categoryService, IMapper mapper, SignInManager<User> signInManager, UserManager<User> userManager, IConfiguration config, IPOCService pocService)
        {
            _userService = userService;
            _cityService = cityService;
            _categoryService = categoryService;
            _mapper = mapper;
            _signInManager = signInManager;
            _userManager = userManager;
            _config = config;
            _pocService = pocService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategoryAndCity()
        {
            var cities = await _cityService.ListAsync();
            var categories = await _categoryService.ListAsync();

            return Ok(new { Categories = categories, Cities = cities });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.AddAsync(model, model.SelectedRole);
                return Ok(result);
            }
            return BadRequest(ModelState.ValidationState);
        }

        [HttpPost("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string token, string email)
        {

            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(email))
            {
                return BadRequest("Please try again.");
            }
            else
            {
                var result = await _userService.ConfirmEmail(token, email);
                if (result)
                {
                    return Ok("Email confirmed, you can login now!");
                }
                else
                {
                    return BadRequest("Please try again!");
                }
            }
        }


        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDto model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                return Unauthorized();
            }
            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
            if (result.Succeeded)
            {
                var roleinfo = await _userManager.GetRolesAsync(user);
                // Create JWT token
                var claims = new[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Azp,string.Join(",",roleinfo)),
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(ClaimTypes.Name, user.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                    };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);

                var token = new JwtSecurityToken(
                        _config["Tokens:Issuer"],
                        _config["Tokens:Audience"],
                        claims,
                        expires: DateTime.UtcNow.AddDays(1),
                        signingCredentials: creds
                    );


                // return OK and the bearer JWT token to client if successfully created
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    user = user
                });
            }
            return BadRequest(ModelState.ValidationState);
        }



        [HttpPost("CashiersLogin")]
        public async Task<IActionResult> CashiersLogin([FromBody] LoginRequestDto model)
        {
            var poc = await _pocService.GetDetailById(model.PocId);
            if (poc != null && poc.Pin == model.PocPin && poc.Active)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                var isEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

                if (user == null || !isEmailConfirmed)
                {
                    return Unauthorized();
                }
                if (user.CashierActive)
                {
                    var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                    if (result.Succeeded)
                    {
                        var roleinfo = await _userManager.GetRolesAsync(user);
                        // Create JWT token
                        var claims = new[]
                        {
                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Azp,string.Join(",",roleinfo)),
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(ClaimTypes.Name, user.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                    };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);

                        var token = new JwtSecurityToken(
                                _config["Tokens:Issuer"],
                                _config["Tokens:Audience"],
                                claims,
                                expires: DateTime.UtcNow.AddDays(1),
                                signingCredentials: creds
                            );


                        // return OK and the bearer JWT token to client if successfully created
                        return Ok(new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo,
                            user = user
                        });
                    }
                    return BadRequest(ModelState.ValidationState);
                }
                return BadRequest("Cashiers is Not active!");
            }
            return BadRequest("Please try again!");
        }
    }
}
