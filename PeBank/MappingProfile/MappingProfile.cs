﻿using AutoMapper;
using PeBank.DataTransferObjects.Services;
using PeBank.Entities;

namespace PeBank.WebAPI
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<Business, BusinessRequestDto>().ReverseMap();
            CreateMap<BusinessCategory, BusinessCategoryDto>().ReverseMap();
            CreateMap<MerchantContact, MerchantContactDto>().ReverseMap();
            CreateMap<POC, POCDto>().ReverseMap();
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<Role, RoleDto>().ReverseMap();
        }

    }
}
